package config

import (
	"fmt"
	"log"

	"github.com/spf13/viper"
)

// Configuration read from file
type Configuration struct {
	Server struct {
		Port string
	}
	Files struct {
		LocalPath string
	}
}

func init() {
	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/files_server/")
	viper.AddConfigPath(".")
}

// New creates a new server base on config
func New() (*Configuration, error) {
	var conf Configuration

	viper.SetDefault("Server.Port", "http")
	viper.SetDefault("Files.LocalPath", "./test_folder")

	err := viper.ReadInConfig()
	if err != nil {
		log.Print("No config found, using default values.", err)
	}

	err = viper.Unmarshal(&conf)
	if err != nil {
		return &conf, fmt.Errorf("Unable to decode into struct, %v", err)
	}

	return &conf, err
}
