package config

import (
	"fmt"
	"os"
	"testing"
)

const checkMark = "\u2713"
const ballotX = "\u2717"

func TestConfigFiles(t *testing.T) {
	t.Run("YAML configuration file", testYamlConfig)
	// t.Run("JSON configuration file", testJSONConfig)
	// t.Run("Default values configuration", testDefaultValues)
}

func testJSONConfig(t *testing.T) {
	jsonTemplate := `{"Server": {"Port": "%s"}, "Files": {"LocalPath": "%s"}}`
	jsonType := "json"
	var c Configuration
	c.Server.Port = "8080"
	c.Files.LocalPath = "./test_folder1"

	testConfig(t, c, jsonTemplate, jsonType)
}

func testYamlConfig(t *testing.T) {
	ymlTemplate := `Server:
    Port: %s
Files:
    LocalPath: %s
`
	ymlType := "yml"
	var c Configuration
	c.Server.Port = "8081"
	c.Files.LocalPath = "./test_folder2"

	testConfig(t, c, ymlTemplate, ymlType)
}

func testDefaultValues(t *testing.T) {
	var c *Configuration
	serverPort := "http"
	folderPath := "./test_folder"

	t.Log("When reading configuration without config files")
	{
		var err error
		c, err = New()
		if err != nil {
			t.Fatal("\t\tUnable to get configuration.", ballotX, err)
		}
	}

	t.Log("Server configuration should take default values")
	{
		if r := c.Server.Port; r == serverPort {
			t.Logf("\t\tShould receive a \"%s\" status. %v", serverPort, checkMark)
		} else {
			t.Errorf("\t\tShould receive a \"%s\" status. %v, got %s", serverPort, ballotX, r)
		}
		if r := c.Files.LocalPath; r == folderPath {
			t.Logf("\t\tShould receive a \"%s\" status. %v", folderPath, checkMark)
		} else {
			t.Errorf("\t\tShould receive a \"%s\" status. %v, got %s", folderPath, ballotX, r)
		}
	}
}

func testConfig(t *testing.T, conf Configuration, fileTemplate string, fileType string) {
	serverPort := conf.Server.Port
	folderPath := conf.Files.LocalPath

	fileContent := fmt.Sprintf(fileTemplate, serverPort, folderPath)
	var c *Configuration

	t.Logf("Given there is a %s file", fileType)
	{
		err := createFile(fileContent, fmt.Sprintf("config.%s", fileType))
		if err != nil {
			t.Fatal("\t\tShould be able to create a config file.", ballotX, err)
		}

	}
	defer os.Remove(fmt.Sprintf("config.%s", fileType))

	t.Log("When reading configuration")
	{
		var err error
		c, err = New()
		if err != nil {
			t.Fatal("\t\tUnable to get configuration.", ballotX, err)
		}
	}

	t.Log("Server configuration should match")
	{
		if r := c.Server.Port; r == serverPort {
			t.Logf("\t\tShould receive a \"%s\" status. %v", serverPort, checkMark)
		} else {
			t.Errorf("\t\tShould receive a \"%s\" status. %v, got %s", serverPort, ballotX, r)
		}
		if r := c.Files.LocalPath; r == folderPath {
			t.Logf("\t\tShould receive a \"%s\" status. %v", folderPath, checkMark)
		} else {
			t.Errorf("\t\tShould receive a \"%s\" status. %v, got %s", folderPath, ballotX, r)
		}
	}

}

func createFile(text string, filename string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	_, err = f.WriteString(text)
	if err != nil {
		f.Close()
		return err
	}
	err = f.Close()
	return err
}
