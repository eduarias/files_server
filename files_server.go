package main

import (
	"files_server/config"
	"log"
	"net/http"
)

var conf *config.Configuration

func init() {
	var err error

	conf, err = config.New()
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	s := &http.Server{
		Addr:    ":" + conf.Server.Port,
		Handler: http.FileServer(http.Dir(conf.Files.LocalPath)),
	}
	log.Printf("Running server on port %s", s.Addr)
	log.Fatal(s.ListenAndServe())
}
